#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:18435382:3ac91c41ccb9237793db4ecb042da7d1f6661ab7; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:17980722:ba2eda07bc7f73080b933cb0c48c88e92872d6e5 EMMC:/dev/block/bootdevice/by-name/recovery 3ac91c41ccb9237793db4ecb042da7d1f6661ab7 18435382 ba2eda07bc7f73080b933cb0c48c88e92872d6e5:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
